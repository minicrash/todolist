import React from "react"

class ToDoList extends React.Component {
  constructor(props) {
    super(props)
    this.props = props
    this.state = {
      todo: null,
      todos: [
        {
          text: "Salade"
        },
        {
          text: "Tomate"
        },
        {
          text: "Oignons"
        }
      ]
    }
  }
  addTodo() {
    this.setState({ todos: [...this.state.todos, { text: this.state.todo }] })
  }
  render() {
    return (
      <div>
        <input type="text" onChange={event => this.setState({ todo: event.target.value })} />
        <button onClick={() => this.addTodo()}>Add Item</button>
        <ul>
          {this.state.todos.map((todo, index) => (
            <li>{todo.text}</li>
          ))}
        </ul>
      </div>
    )
  }
}

export default ToDoList
